# Proton-clang

### How to use

Add this to kernel compiler script
```
TC_DIR="$(pwd)/clang-neutron"

if ! [ -d "$TC_DIR" ]; then
    echo "Neutron Clang not found! Cloning to $TC_DIR..."
    if ! git clone --depth=1 -b 19 https://gitlab.com/HinohArata/neutron-clang.git "$TC_DIR"; then
        echo "Cloning failed! Aborting..."
        exit 1
    fi
fi

export PATH="$TC_DIR/bin:$PATH"
```
